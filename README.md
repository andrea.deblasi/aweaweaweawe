# Informazioni base sul progetto(Chiedi accesso a zollino)
https://docs.google.com/document/d/1Qw2qkJpM-SOnqkL3-4XRkQP8xmX5CHPpQ7QYksSwZCQ/edit?usp=sharing


# Obiettivi del progetto
    - Prima settimana 28/01 - 4/02 (2024)
        FrontEnd:
            Homepage con elementi grafici con funzionalità di base annesse.
            Sign in/Sign up con elementi grafici di base e funzionalità coinvolte per l'acquisizione dei dati.
        BackEnd:
            Realizzazione database/php
                tabelle relative ai dati dell'utente base
                tabelle relative alla registrazione nuovo utente
                tabelle relative ai dati dell'utente organizzatore
                tabelle relative alla registrazione di nuovo organizzatore





